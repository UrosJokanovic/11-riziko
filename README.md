# Riziko :map:

Društvena igra na tabli za pet igrača. Na početku igre svaki igrač dobija zadatak i teritorije koje mu pripadaju. Cilj igre je prvi ispuniti izvučeni zadatak. Postoje tri vrste zadataka - unišiti određenog igrača, okupiraj određene regione i okupiraj određeni broj teritorija. Svaki zadatak ostaje tajan do kraja igre i igrač radi na ispunjenju istog.

# Korisćene biblioteke :books:
* Qt >= 5.12 

# Instalacija potebnih biblioteka :hammer:
## Qt i Qt Creator
1. `$ sudo apt install qtcreator`
2. `$ sudo apt install build-essential`
3. `$ sudo apt install qt5-default`
4. `$ sudo apt install qtmultimedia5-dev`

# Kloniranje i pokretanje :wrench:
1. Kloniranje repozitorijuma: `$ git clone https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2022-2023/11-riziko`
2. Otvaranje projekta u QtCreator-u
3. Pokretanje na dugme `RUN` u donjem levom uglu ili pritiskom `CTRL + R`. 

# Demo snimak :movie_camera:
https://www.youtube.com/watch?v=3613MxJecz0

# Developers :computer:

<ul>
    <li><a href="https://gitlab.com/tamtam11">Tamara Tomić 122/2017</a></li>
    <li><a href="https://gitlab.com/UrosJokanovic">Uroš Jokanović 179/2017</a></li>
    <li><a href="https://gitlab.com/OgnjenJankovic">Ognjen Janković 88/2017</a></li>
    <li><a href="https://gitlab.com/majamajaat">Maja Todorović 177/2017</a></li>
    <li><a href="https://gitlab.com/nebojsa-cvetkovic">Nebojša Cvetković 157/2017</a></li>
</ul>
