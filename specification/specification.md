# Dijagram klasa
[Dijagram klasa](class_diagram_final.pdf)

# Dijagram slucajeva upotrebe
[Dijagram slucajeva upotrebe](use_case_diagram_final.pdf)

# Igranje jedne partije igre 

**Kratak opis:** \*  Kada se svih pet igraca registruje, igra zapocinje. Igraci na pocetku partije dobijaju kartice sa zadacima. Igra je gotova onog trenutka kada prvi igrac ispuni svoj zadatak. Kad se igra zavrsi,
ispisuje se ime pobednika.

**Akteri** \* Igraci - igraju jednu partiju.

**Preduslovi:** \* Igraci su se pridruzili igri.

**Postuslovi:** \* Informacije o odigranoj partiji se trajno brisu.

**Osnovni tok:**
 1. Inicijalizuje se partija sa svim potrebnim parametrima
 2. Igracu se prikazuje prozor za igru
  <br>2.1 Dodeljuju se tenkici igracima
  <br> 2.2 Dodeljuju se zadaci igracima 
  <br> 2.3 Dodeljuju se teritorije igracima
 3. Nulta runda pocinje i igraci redom postavljaju proizvoljno tenkice na svoje teritorije
 4. Prva runda pocinje nakon sto svi igraci postave sve tenkice 
  <br> 4.1 Svaki igrac ima pravo da napada dok god je to moguce
   <br> &emsp; 4.1.1 Igrac moze da napada dok god ima vise od jednog tenkica na teritorijama 
 5. Klikom na dugme "PLAY MOVE" se otvara prozor "attack"
  <br> 5.1. Iz opadajuce liste, igrac bira sa koje svoje teritorije zeli da napadne
	<br> &emsp; 5.1.1. Na osnovu izabrane teritorije, bira teritoriju koju moze da napadne
	<br> &emsp; 5.1.2. Napad se vrsi pritiskom na dugme "ATTACK"
  <br >5.2. Kockice bacaju napadac i branilac
	<br> &emsp; 5.2.1 Broj kockica zavisi od broja tenkica koje napadac ima na toj teritoriji (maksimalno 3)
  <br> &emsp; 5.2.2 Branilac baca onoliko kockica koliko ima tenkica na teritoriji (maksimalno 3) 
   	<br> &emsp; &emsp; 5.2.2.1 Napadac mora da ima bar dva tankica da bi napao 
		<br> &emsp; &emsp;	5.2.2.1.1 Bacene kockie se redom uporedjuju od najvece ka najmanjoj 
		<br> &emsp; &emsp;	5.2.2.1.2 Ukoliko se porede dva ista broja, branilac pobedjuje 
		<br> &emsp; &emsp;	5.2.2.1.3 U zavisnosti od poredjenja kockica igraci gube odredjeni broj tenkica(maksimalno 3) 
		<br> &emsp; &emsp;	5.2.2.1.4 Napadacu uvek mora ostati jedan tenkic na teritoriji sa koje je napadao
 6. Ukoliko napadac osvoji teritoriju moze dobiti karticu pritiskom na dugme "Draw card" cime mu se onemogucuju dalji napadi
  <br>6.1. Nakon izvlacenja kartice, igrac moze videti sve kartice koje u datom trenutku poseduje pritiskom na dugme "CARDS"
   <br> &emsp; 6.1.1 Pritiskom na dugme "EXCHANGE CARDS", igrac moze zameniti kartice za dodatne tenkice
      <br> &emsp; &emsp;  6.1.1.1 Tri kartice "INFANTRY" donose 4 nova tenkica 
      <br> &emsp; &emsp;   6.1.1.2 Tri kartice "CAVALRY" donose 6 novih tenkica 
      <br> &emsp; &emsp;   6.1.1.3 Tri kartice tipa "CANNON" donose 8 novih tenkica 
       <br> &emsp; &emsp;  6.1.1.4 Tri razlicite kartice donose 10 novih tenkica 
 7. Nakon odigranog poteza igrac klikne "Next Player" da bi igrao naredni igrac
   <br> 7.1 Za svakog igraca vaze prethodni potezi do kraja prve runde 
 8. Nakon sto svi igraci zavrse svoj prvi potez krece nova runda 
  <br> 8.1 Na pocetku svog poteza u novoj rundi igraci dobijaju tenkice
   <br> &emsp; 8.1.1 Broj novih tenkica se dodeljuje na osnovu broja teritorija (broj teritorija se deli se tri)
   <br> &emsp; 8.1.2 Igrac ne moze odigrati potez dok ne rasporedi tenkice
   8.2 Svi ostali koraci su isti kao i u prvoj rundi 
 9. Zavrsetak partije 
  <br> 9.1 Igrac zavrsava partiju ukoliko ostane bez teritorija 
  <br> 9.2 Partija je zavrsena ukoliko je neki igrac ispunio svoj zadatak 
 10. Ispis pobednika i izlazak iz igre

**Alternativni tokovi:** 
  
**Specijalni zahtevi:**

[Dijagram sekvence: 'Igranje jedne partije'](playing_one_game_sequence_diagram.pdf)

# Dodeljivanje i rasporedjivanje armija

**Kratak opis**: Svaki put kada igrac bude na potezu, on dobija tenkove koje treba da rasporedi po svojim teritorijama. Prilikom prvog poteza u partiji, igracu se na pocetku partije dodeljuje 28 tenkova. Igrac treba da rasporedi 21 tenk po svojim teritorijama, jer je preostalih 7 automatski rasporedjeno (na svaku teritoriju igraca po 1). Svaki sledeci put kada bude njegov potez, igrac dobija dodatne tenkove (u zavisnosti od toga koliko teritorija poseduje u tom trenutku) i rasporedjuje ih. Pored toga, na kraju svakog poteza u kom je osvojio najmanje jednu teritoriju, igrac ima pravo da vuce karticu. Kada sakupi 3 kartice moze ih zameniti za dodatne tenkove i njih rasporediti. 

**Akteri**:
Igrac koji je na potezu.

**Preduslovi**: Aplikacija je aktivna i partija je u toku.

**Postuslovi**: 1) U slucaju standardne dodele tenkova na pocetku poteza: Igrac je rasporedio svoje tenkove i spreman je da izvrsi napad.
		2) U slucaju razmene kartica za tenkove : Igrac je rasporedio svoje tenkove i moze da preda potez narednom igracu.

**Osnovni tok**:
0. Prvi put u partiji kada je igrac na potezu (nulti potez), dodeljuje mu se 28 tenkova da ih rasporedi. 
   Prilikom prvog poteza nema dodele dodatnih tenkova, vec se za napad koriste oni dodeljeni i rasporedjeni u nultom potezu. 
1. Svaki sledeci put kada je igrac na potezu, on dobija dodatne tenkove na osnovu broja teritorija koje u tom trenutku poseduje. 
	* 1.1. Broj tenkova koji se dodeljuju igracu se odredjuje po sledecoj formuli: broj teritorija koje igrac u tom trenutku poseduje se podeli sa 3 i zatim zaokruzi na manju celobrojnu vrednost.
	* 1.2. Ukoliko se igrac zatim odluci da napada i tom prilikom osvoji teritoriju, on moze prebaciti dodatne tenkove sa teritorija koje poseduje na novoosvojenu teritoriju (prilikom osvajanja automatski se na novoosvojenu teritoriju prebacuje 1 tenk sa teritorije sa iz koje je izvrsen napad)
	* 1.3. Na kraju svakog poteza igrac moze zameniti kartice za dodatne tenkove.
		* 1.3.1. Za dobijanje dodatnih tenkova potrebne su 3 kartice
			* 1.3.1.1. Broj tenkova koji se dobijaju zavisi od kartica koje igrac menja za njih, razmena se vrsi u skladu sa tabelom prilozenom u dodatnim informacijama
		* 1.3.2. Nakon zamene kartica za tenkove, igrac novodobijene tenkove rasporedjuje po svojim teritorijama. 

**Alternativni tokovi**: /

**Podtokovi**: /

**Specijalni zahtevi**: Igrac na potezu mora biti povezan na internet i posedovati kod sebe klijent aplikacije.
                        Server aplikacije mora biti pokrenut u trenutku pokretanja partije.
                        Igrac mora biti u partiji.

**Dodatne informacije**:

Tabela kartica:

| Kartice                 	| Broj tenkova 	|
|-------------------------	|:------------:	|
| 3 pesadinca             	|       4      	|
| 3 konjanika             	|       6      	|
| 3 topa                  	|       8      	|
| 3 razlicite kartice     	|      10      	|

[Dijagram sekvence: 'Dodeljivanje i rasporedjivanje armija'](assign_and_deploy_diagram.pdf)

# Podešavanje jedne partije igre

**Kratak opis**: Vrši se odabir  opcije "JOIN GAME" iz glavnog menija. Nakon toga svaki od igrača bira svoje ime i boju i priključuje se igri.

**Akteri**:  Igrači

**Preduslovi**:  Aplikacija prikazuje glavni meni.

**Postuslovi**:  Igra je pokrenuta.

**Osnovni tok**:
 1.  Igrač unosi ime i vrši se provera da li je neko već uzeo to ime.
 2.  Igrač bira boju.
 3.  Priključuje se igri klikom na dugme "ADD PLAYER".
 4.  Kada se priključe svi igrači, klikom na dugme "START" započinje sama igra.

**Alternativni tokovi**: /

**Podtokovi**: /

**Specijalni zahtevi**:  /

**Dodatne informacije**: /

[Dijagram sekvence: 'Podesavanje jedne partije'](settings_diagram.pdf)
