# Igranje jedne partije igre 

**Kratak opis:** \*  Kada se svih pet igraca registruje, igra zapocinje. Igraci na pocetku partije dobijaju kartice sa zadacima. Igra je gotova onog trenutka kada prvi igrac ispuni svoj zadatak. Kad se igra zavrsi,
ispisuje se ime pobednika.

**Akteri** \* Igraci - igraju jednu partiju.

**Preduslovi:** \* Igraci su se pridruzili igri.

**Postuslovi:** \* Informacije o odigranoj partiji se trajno brisu.

**Osnovni tok:**
 1. Inicijalizuje se partija sa svim potrebnim parametrima
 2. Igracu se prikazuje prozor za igru
   2.1 Dodeljuju se tenkici igracima
   2.2 Dodeljuju se zadaci igracima 
   2.3 Dodeljuju se teritorije igracima
 3. Nulta runda pocinje i igraci redom postavljaju proizvoljno tenkice na svoje teritorije
 4. Prva runda pocinje nakon sto svi igraci postave sve tenkice 
   4.1 Svaki igrac ima pravo da napada dok god je to moguce
    4.1.1 Igrac moze da napada dok god ima vise od jednog tenkica na teritorijama 
 5. Klikom na dugme "PLAY MOVE" se otvara prozor "attack"
  5.1. Iz opadajuce liste, igrac bira sa koje svoje teritorije zeli da napadne
	5.1.1. Na osnovu izabrane teritorije, bira teritoriju koju moze da napadne
	5.1.2. Napad se vrsi pritiskom na dugme "ATTACK"
  5.2. Kockice bacaju napadac i branilac
	5.2.1 Broj kockica zavisi od broja tenkica koje napadac ima na toj teritoriji (maksimalno 3)
    5.2.2 Branilac baca onoliko kockica koliko ima tenkica na teritoriji (maksimalno 3) 
   		5.2.2.1 Napadac mora da ima bar dva tankica da bi napao 
			5.2.2.1.1 Bacene kockie se redom uporedjuju od najvece ka najmanjoj 
			5.2.2.1.2 Ukoliko se porede dva ista broja, branilac pobedjuje 
			5.2.2.1.3 U zavisnosti od poredjenja kockica igraci gube odredjeni broj tenkica(maksimalno 3) 
			5.2.2.1.4 Napadacu uvek mora ostati jedan tenkic na teritoriji sa koje je napadao
 6. Ukoliko napadac osvoji teritoriju moze dobiti karticu pritiskom na dugme "Draw card" cime mu se onemogucuju dalji napadi
  6.1. Nakon izvlacenja kartice, igrac moze videti sve kartice koje u datom trenutku poseduje pritiskom na dugme "CARDS"
    6.1.1 Pritiskom na dugme "EXCHANGE CARDS", igrac moze zameniti kartice za dodatne tenkice
        6.1.1.1 Tri kartice "INFANTRY" donose 4 nova tenkica 
        6.1.1.2 Tri kartice "CAVALRY" donose 6 novih tenkica 
        6.1.1.3 Tri kartice tipa "CANNON" donose 8 novih tenkica 
        6.1.1.4 Tri razlicite kartice donose 10 novih tenkica 
 7. Nakon odigranog poteza igrac klikne "Next Player" da bi igrao naredni igrac
   7.1 Za svakog igraca vaze prethodni potezi do kraja prve runde 
 8. Nakon sto svi igraci zavrse svoj prvi potez krece nova runda 
   8.1 Na pocetku svog poteza u novoj rundi igraci dobijaju tenkice
    8.1.1 Broj novih tenkica se dodeljuje na osnovu broja teritorija (broj teritorija se deli se tri)
    8.1.2 Igrac ne moze odigrati potez dok ne rasporedi tenkice
   8.2 Svi ostali koraci su isti kao i u prvoj rundi 
 9. Zavrsetak partije 
   9.1 Igrac zavrsava partiju ukoliko ostane bez teritorija 
   9.2 Partija je zavrsena ukoliko je neki igrac ispunio svoj zadatak 
 10. Ispis pobednika i izlazak iz igre

**Alternativni tokovi:** 
  
**Specijalni zahtevi:**
