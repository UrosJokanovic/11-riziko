FROM ubuntu:latest

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update -y 
RUN apt-get install qtcreator build-essential qtmultimedia5-dev cmake -y

COPY . /Desktop/RS/11-riziko
WORKDIR . /Desktop/RS/11-riziko
RUN mkdir build

WORKDIR /Desktop/RS/11-riziko/build
RUN cmake ..
RUN make

CMD ["./risk"]
