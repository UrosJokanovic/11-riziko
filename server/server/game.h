#ifndef GAME_H
#define GAME_H

#include <iostream>
#include <QVector>
#include <QSet>
#include <QTimer>
#include <QRandomGenerator>
#include "region.h"
#include "player.h"
#include "card.h"
#include "territory.h"

class Card;
class Territory;
class Settings;
class Player;
class Region;

class Game {

public:
    Game();

    bool checkGoal(Player*);
    QVector<int> attack(Territory* , Territory*);

    const QVector<Player*> &players() const;
    const QSet<Card*> &cards() const;
    const QVector<Region*> &regions() const;
    const QSet<Territory*> &territories() const;
    Player *currentPlayer() const;
    void setCurrentPlayer(Player *newCurrentPlayer);
    std::int32_t addTanksToTerritory(std::int32_t, Territory*);

    void setTerritories(QSet<Territory*>);
    void setRegions(QVector<Region*>);
    void setPlayers(QVector<Player*>);
    void setCards(QSet<Card*>);

    ~Game();

private:
    QVector<Player*> m_players;
    QSet<Card*> m_cards;
    QSet<Territory*> m_territories;
    QVector<Region*> m_regions;
    Player* m_currentPlayer;
};



#endif // GAME_H
