#ifndef TCPSERVER_H
#define TCPSERVER_H

#include <QWidget>
#include <QAbstractSocket>
#include <QMessageBox>

#include "../game.h"
#include "../initializer.h"
//#include "../territory.h"


QT_BEGIN_NAMESPACE
namespace Ui { class TcpServer; }
QT_END_NAMESPACE

class QTcpServer;
class QTcpSocket;

class TcpServer : public QWidget
{
    Q_OBJECT

public:
    TcpServer(QWidget *parent = nullptr);
    ~TcpServer();

signals:
    void tryToConnect(QTcpSocket*);
    void tryToStart(QTcpSocket*, int);
    void printNumberOfPlayers(QTcpSocket*);
    void playerLeftLobby(QTcpSocket*);
    void playerReady(QTcpSocket*, QString, QString);
    void removePlayer(QTcpSocket*);
    void startInitiated(QTcpSocket*);
    void updateGameUi(QTcpSocket*);
    void taskColorAndInfo(QTcpSocket*);
    void requestEnable(QTcpSocket*);
    void instanceLoop(QTcpSocket*);
    void updateEveryoneWaiting(QTcpSocket*, QString);
    void nextPlayer(QTcpSocket*);
    void requestNeighbours(QTcpSocket*, QString);
    void throughDices(QTcpSocket*,QString, QString, QString, QString);
    void attackUpdate(QTcpSocket*,QString, QString, QString, QString);
    void attackWon(QTcpSocket*,QString, QString, QString, QString, QString);

private slots:
    void newPlayer();
    void playerDisconnected();
    void messageReceived();
    void errorHandler(QAbstractSocket::SocketError);
    void on_disconnect_server_clicked();

private:
    Ui::TcpServer *ui;
    QTcpServer *m_server;
    QList<QTcpSocket*> m_clients;
    QVector<Player*> m_players;
    QHash<QTcpSocket*, Player*> m_clientToPlayer;
    QHash<Player*, QTcpSocket*> m_playerToClient;
    QHash<Player*, Player*> m_playerToPlayer;

    bool taskCardAndColorInfo = false;
    bool nextPlayerp = false;
    bool alreadyInLoop = false;
    bool instanced = false;
    bool initializedGame = false;
    bool gameInitiated = false;
    bool lobbyLocked = false;
    int32_t m_numOfConnectedPlayers = 0;
    int32_t m_numOfReadyPlayers = 0;
    int32_t m_numOfStartedGames = 0;

    void listenToPort();
    void printIpAddressAndPort();
    void parseMessage(QString, QTcpSocket*);
    void sendMessage(QTcpSocket*, const QString&);
    void disconnectButtonHandler(QList<QTcpSocket*>);
};
#endif // TCPSERVER_H
