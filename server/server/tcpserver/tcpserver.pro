QT       += core gui
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    tcpserver.cpp \
    ../card.cpp \
    ../cardtask.cpp \
    ../destroyplayercard.cpp \
    ../game.cpp \
    ../initializer.cpp \
    ../player.cpp \
    ../region.cpp \
    ../regionoccupycard.cpp \
    ../settings.cpp \
    ../territory.cpp \
    ../territoryoccupycard.cpp

HEADERS += \
    tcpserver.h \
    ../card.h \
    ../cardtask.h \
    ../destroyplayercard.h \
    ../game.h \
    ../initializer.h \
    ../player.h \
    ../region.h \
    ../regionoccupycard.h \
    ../settings.h \
    ../territory.h \
    ../territoryoccupycard.h

FORMS += \
    tcpserver.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
