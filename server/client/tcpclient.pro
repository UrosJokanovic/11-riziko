QT       += core gui widgets network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    main.cpp \
    tcpclient/tcpclient.cpp \
    ../server/card.cpp \
    ../server/cardtask.cpp \
    ../server/destroyplayercard.cpp \
    ../server/game.cpp \
    ../server/initializer.cpp \
    ../server/mainwindow.cpp \
    ../server/player.cpp \
    ../server/region.cpp \
    ../server/regionoccupycard.cpp \
    ../server/settings.cpp \
    ../server/territory.cpp \
    ../server/territoryoccupycard.cpp


HEADERS += \
    tcpclient/tcpclient.h \
    ../server/card.h \
    ../server/cardtask.h \
    ../server/destroyplayercard.h \
    ../server/game.h \
    ../server/initializer.h \
    ../server/mainwindow.h \
    ../server/player.h \
    ../server/region.h \
    ../server/regionoccupycard.h \
    ../server/settings.h \
    ../server/territory.h \
    ../server/territoryoccupycard.h

FORMS += \
    tcpclient/tcpclient.ui \
    ../server/mainwindow.ui

RESOURCES += \
    ../server/images.qrc

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
