#ifndef TCPCLIENT_H
#define TCPCLIENT_H

#include <QWidget>
#include <QAbstractSocket>
#include <QMessageBox>

#include <iostream>

#include "../server/mainwindow.h"
#include "../server/player.h"
#include "../server/territory.h"

class QTcpSocket;

QT_BEGIN_NAMESPACE
namespace Ui { class TcpClient; }
QT_END_NAMESPACE

class TcpClient : public QWidget
{
    Q_OBJECT

public:
    TcpClient(QWidget *parent = nullptr);
    ~TcpClient();

private slots:
    void readMessage();
    void sendMessage(QString);
    void on_connect_clicked();
    void connectedToServer();
    void on_disconnect_clicked();
    void disconnectedFromServer();

signals:
    void connection(QString);
    void startMenu();
    void settings();
    void closeGame();
    void newPlayerInLobby(QString);
    void lockLobby();
    void unlockLobby();
    void disableJoin();
    void nameUnavailable();
    void colorUnavailable();
    void disableNameAndColor();
    void enableStart();
    void enableBack();
    void closeClient();
    void removeColor(QString);
    void startInitiated();
    void allPlayersReady(bool);
    void updateGameUi();
    void playerColorButton(QString, QString);
    void initUpdate(QString);
    void taskCardColor(QString);
    void getTerritories();
    void startTurn();
    void waitingUpdate(QString);
    void returnBreak();
    void updateCbTo(QString);
    void throwDiceUpdate(QString, QString, QString, QString, QString);
    void attackUpdate(QString, QString, QString, QString);
    void territoryWon(QString, QString, QString, QString, QString);
    void attackWon(QString, QString, QString, QString, QString);

private:
    Ui::TcpClient *ui;
    QTcpSocket *m_socket;
    QString m_userData;
    QString m_connectionData;
    QString currentTerritories;
    QString currentTanksInOrder;
    QString taskCardPath;
    QString m_playerColor;
//    QString currentNumberOfTanks;
    Player* m_player;
    MainWindow* m_mainWindow = nullptr;
    QPushButton* playerButton;

    bool cbToDone = false;
    bool loopInst = false;
    bool reqapproved = false;
    bool colorFirst = false;
    bool firstTime = true;
    bool firstRound = true;
    bool playerMove = false;
    int32_t i = 0;
    int32_t playerNameButtonSet = 0;
    int32_t numOfTanks = 0;
    void setIpAdressAndPort(Ui::TcpClient*);
    void parseMessage(const QString&);
    void guiChange(QAbstractSocket::SocketState state);
    void closeEvent(QCloseEvent *event);
};
#endif // TCPCLIENT_H
