#include "catch.hpp"

#include "../riziko/card.h"
#include "../riziko/territory.h"
#include "../riziko/region.h"

Player *player = new Player("Player", Color::RED);

Territory* terr1 = new Territory("territory1");
Territory* terr2 = new Territory("territory2");
Territory* terr3 = new Territory("territory3");

TEST_CASE("Card constructor check", "[card]") {
    int cardId = 1;
    Territory* cardTerritory = terr1;
    Card* card = new Card(cardId, cardTerritory);

    REQUIRE(card->id()==cardId);
    REQUIRE(card->territory()==cardTerritory);
}

TEST_CASE("Set card type", "[card]") {

    Card* card = new Card(2, terr2);

    SECTION("INFANTRY") {
        card->setCardType(CardType::INFANTRY);
        CHECK(card->cardType()==CardType::INFANTRY);
    }

    SECTION("CAVALRY") {
        card->setCardType(CardType::CAVALRY);
        CHECK(card->cardType()==CardType::CAVALRY);
    }

    SECTION("CANNON") {
        card->setCardType(CardType::CANNON);
        CHECK(card->cardType()==CardType::CANNON);
    }
}

TEST_CASE("Region constructor check", "[region]") {
    QSet<Territory*> territories;
    territories.insert(terr1);
    territories.insert(terr2);
    territories.insert(terr3);

    Region* r = new Region("Region", territories);

    REQUIRE(r->name()=="Region");
    REQUIRE(r->territories()==territories);
}

TEST_CASE("Territory constructor check", "[territory]") {
    Territory* t = new Territory("territory");
    CHECK(t->name()=="territory");
}

TEST_CASE("Set number of tanks", "[territory]") {
    int value = 5;
    terr1->setNumOfTanks(value);

    int return_value = terr1->numOfTanks();

    REQUIRE(value==return_value);
}

TEST_CASE("Increase number of tanks", "[territory]") {
    int current = 10;
    int increment = 4;
    terr1->setNumOfTanks(current);

    terr1->increaseNumOfTanks(increment);

    int return_value = terr1->numOfTanks();
    REQUIRE(current+increment==return_value);

    SECTION("Increase by 0") {
        current = 10;
        terr1->setNumOfTanks(current);

        increment = 0;
        terr1->increaseNumOfTanks(increment);

        return_value = terr1->numOfTanks();
        REQUIRE(current+increment==return_value);
    }
}

TEST_CASE("Decrease number of tanks", "[territory]") {
    int current = 10;
    int decrement = 6;
    terr1->setNumOfTanks(current);

    terr1->decreaseNumOfTanks(decrement);

    int return_value = terr1->numOfTanks();
    REQUIRE(current-decrement==return_value);

    SECTION("Decrease by 0") {
        current = 10;
        terr1->setNumOfTanks(current);

        decrement = 0;
        terr1->decreaseNumOfTanks(decrement);

        return_value = terr1->numOfTanks();
        REQUIRE(current-decrement==return_value);
    }
}

TEST_CASE("Set neighbor territories", "[territory]") {
    auto territories = new QSet<Territory*>;
    territories->insert(terr2);
    territories->insert(terr3);

    terr1->setNeighborTerritories(*territories);

    REQUIRE(terr1->neighborTerritories()==*territories);
}

TEST_CASE("Set owner for territory", "[territory]") {
    terr1->setOwner(player);
    REQUIRE(terr1->owner()==player);
}
