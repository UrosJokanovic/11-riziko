#include "destroyplayercard.h"

DestroyPlayerCard::DestroyPlayerCard() = default;

auto DestroyPlayerCard::playerToDestroy() const -> Player * { return m_playerToDestroy; }

auto DestroyPlayerCard::checkTask(Player *playerToCheck) const -> bool {
  if (playerToCheck == playerToCheck->playerToDestroy()) {
    if (playerToCheck->territories().size() == 22) {
      return true;
    }
  } else {
    if (playerToCheck->playerToDestroy()->territories().size() == 0) {
      return true;
    }
  }

  return false;
}

DestroyPlayerCard::DestroyPlayerCard(const QString &path, Player *playerToDestroy)
  : CardTask(path), m_playerToDestroy(playerToDestroy) {}

DestroyPlayerCard::~DestroyPlayerCard() = default;
