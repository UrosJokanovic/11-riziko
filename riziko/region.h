#ifndef REGION_H
#define REGION_H
#include <QSet>
#include <QString>

#include "player.h"
#include "territory.h"

class Region {
 public:
  Region();
  Region(
      QString name, const QSet<Territory *> &territories, std::int32_t numOfTanksPerRound);
  Region(QString name, const QSet<Territory *> &territories);

  const QString &name() const;
  const QSet<Territory *> &territories() const;
  std::int32_t numOfTanksPerRound() const;
  bool regionOwner(Player *);

 private:
  QString m_name;
  QSet<Territory *> m_territories;
  std::int32_t m_numOfTanksPerRound;
};

#endif  // REGION_H
