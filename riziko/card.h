#ifndef CARD_H
#define CARD_H

#include "iostream"
#include "cstdint"

enum class CardType {
  CAVALRY,
  CANNON,
  INFANTRY,
};

class Territory;

class Card {
 public:
  Card();
  Card(std::int32_t id, Territory *t);

  Territory *territory() const;
  CardType cardType() const;
  std::int32_t id() const;

  void setCardType(const CardType &cardType);

  ~Card();

 private:
  std::int32_t m_id;
  Territory *m_territory;
  CardType m_cardType;
};

#endif  // CARD_H
