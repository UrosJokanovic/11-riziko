#include "cardtask.h"

#include <utility>

CardTask::CardTask() = default;

auto CardTask::path() const -> const QString& { return m_path; }

CardTask::CardTask(QString path) : m_path(std::move(path)) {}
