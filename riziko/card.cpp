#include "card.h"

#include "cstdint"
#include "territory.h"

Card::Card() = default;

Card::Card(int id, Territory *territory) : m_id(id), m_territory(territory) {}

auto Card::territory() const -> Territory * { return m_territory; }

auto Card::cardType() const -> CardType { return m_cardType; }

auto Card::id() const -> std::int32_t { return m_id; }

void Card::setCardType(const CardType &cardType) { m_cardType = cardType; }

Card::~Card() = default;
