#include "region.h"

#include <utility>

Region::Region() = default;

Region::Region(QString name, const QSet<Territory *> &territories, int32_t numOfTanksPerRound)
  : m_name(std::move(name)), m_territories(territories), m_numOfTanksPerRound(numOfTanksPerRound) {}

Region::Region(QString name, const QSet<Territory *> &territories)
  : m_name(std::move(name)), m_territories(territories) {}

auto Region::name() const -> const QString & { return m_name; }

auto Region::territories() const -> const QSet<Territory *> & { return m_territories; }

auto Region::numOfTanksPerRound() const -> std::int32_t { return m_numOfTanksPerRound; }

auto Region::regionOwner(Player *player) -> bool {
  for (auto &territory : m_territories)
    if (territory->owner() != player) return false;

  return true;
}
