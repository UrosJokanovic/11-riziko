#include "player.h"

#include <utility>

#include "card.h"
#include "territory.h"

Player::Player() = default;

Player::Player(QString name, Color color) : m_name(std::move(name)), m_color(color) {
  m_cards = QVector<Card *>();
  m_territories = QSet<Territory *>();
}

auto Player::name() const -> const QString & { return m_name; }

void Player::setName(const QString &newName) { m_name = newName; }

auto Player::color() const -> Color { return m_color; }

void Player::setColor(Color newColor) { m_color = newColor; }

auto Player::numOfTerritories() const -> std::int32_t { return m_territories.size(); }

auto Player::playerToDestroy() const -> Player * { return m_playerToDestroy; }

void Player::setPlayerToDestroy(Player *newPlayerToDestroy) {
  m_playerToDestroy = newPlayerToDestroy;
}

void Player::addCard(Card *newCard) { m_cards.append(newCard); }

void Player::removeCard(Card *card) {
  for (auto &c : m_cards)
    if (c->id() == card->id()) m_cards.removeOne(c);
}

void Player::addTerritory(Territory *newTerritory) { m_territories.insert(newTerritory); }

void Player::removeTerritory(Territory *wantedTerritory) { m_territories.remove(wantedTerritory); }

void Player::removeAllTerritories() { m_territories.clear(); }

auto Player::getTanks(Card *card1, Card *card2, Card *card3) -> int {
  if (card1->id() == card2->id() && card1->id() == card3->id()) {
    return -1;
  }

  if (card1->cardType() == card2->cardType() && card1->cardType() == card3->cardType() &&
      card1->cardType() == CardType::INFANTRY)
    return 4;

  if (card1->cardType() == card2->cardType() && card1->cardType() == card3->cardType() &&
      card1->cardType() == CardType::CAVALRY)
    return 6;

  if (card1->cardType() == card2->cardType() && card1->cardType() == card3->cardType() &&
      card1->cardType() == CardType::CANNON)
    return 8;

  if (card1->cardType() != card2->cardType() && card2->cardType() != card3->cardType() &&
      card1->cardType() != card3->cardType())

    return 10;

  return -1;
}

void Player::addCardTask(CardTask *cardTask) { m_cardTask = cardTask; }

auto Player::cardTask() const -> CardTask * { return m_cardTask; }

auto Player::territories() const -> const QSet<Territory *> & { return m_territories; }

void Player::setTerritories(const QSet<Territory *> &newTerritories) {
  m_territories = newTerritories;
}

auto Player::cards() const -> const QVector<Card *> & { return m_cards; }

void Player::setCards(const QVector<Card *> &newCards) { m_cards = newCards; }

void Player::setNumOfTanks(int num) { m_numOfTanksToAdd = num; }

auto Player::getNumOfTanks() -> int { return m_numOfTanksToAdd; }

void Player::moveTanks(Territory *origin, Territory *destination, int tanksNum) {
  Territory *org = (*m_territories.find(origin));
  org->decreaseNumOfTanks(tanksNum);

  Territory *dest = (*m_territories.find(destination));
  dest->increaseNumOfTanks(tanksNum);
}

auto Player::toString() -> QString {
  QString s;
  switch (m_color) {
    case Color::RED:
      s = "RED";
      break;
    case Color::GREEN:
      s = "GREEN";
      break;
    case Color::BLUE:
      s = "BLUE";
      break;
    case Color::YELLOW:
      s = "YELLOW";
      break;
    case Color::PURPLE:
      s = "PURPLE";
      break;
  }
  return s;
}
