#include <QApplication>

#include "game.h"
#include "mainwindow.h"

auto main(int argc, char *argv[]) -> int {
  QApplication a(argc, argv);
  MainWindow w;
  w.show();
  return a.exec();
}
