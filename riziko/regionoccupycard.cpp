#include "regionoccupycard.h"

#include <iostream>

RegionOccupyCard::RegionOccupyCard() = default;

RegionOccupyCard::RegionOccupyCard(const QString &path, QVector<Region *> &regions,
                                   QVector<Region *> &allRegions, int32_t numOfRegions)
  : CardTask(path), m_regions(regions), m_allRegions(allRegions), m_numOfRegions(numOfRegions) {}

auto RegionOccupyCard::regions() const -> QVector<Region *> { return m_regions; }

auto RegionOccupyCard::numOfRegions() const -> std::int32_t { return m_numOfRegions; }

auto RegionOccupyCard::checkTask(Player *playerToCheck) const -> bool {
  int num = 0;

  for (auto r : m_regions) {
    for (auto t : r->territories()) {
      if (!playerToCheck->territories().contains(t)) {
        return false;
      }
    }
    num += 1;
  }

  bool oneMore = true;
  for (auto r : m_allRegions) {
    oneMore = true;
    if (!m_regions.contains(r)) {
      for (auto t : r->territories()) {
        if (!playerToCheck->territories().contains(t)) {
          oneMore = false;
        }
      }
      if (oneMore) {
        num += 1;
        break;
      }
    }
  }

  if (num == m_numOfRegions) {
    return true;
  }

  return false;
}

auto RegionOccupyCard::allRegions() const -> QVector<Region *> { return m_allRegions; }

void RegionOccupyCard::setAllRegions(const QVector<Region *> &allRegions) {
  m_allRegions = allRegions;
}
