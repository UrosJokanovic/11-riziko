#include "territoryoccupycard.h"

#include "player.h"

TerritoryOccupyCard::TerritoryOccupyCard() = default;

auto TerritoryOccupyCard::numOfTerritories() const -> std::int32_t { return m_numOfTerritories; }

auto TerritoryOccupyCard::checkTask(Player *playerToCheck) const -> bool {
  if (playerToCheck->territories().size() >= m_numOfTerritories) {
    return true;
  } else {
    return false;
  }
}

TerritoryOccupyCard::TerritoryOccupyCard(const QString &path, int32_t numOfTerritories)
  : CardTask(path), m_numOfTerritories(numOfTerritories) {}
