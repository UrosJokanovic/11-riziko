#ifndef REGIONOCCUPYCARD_H
#define REGIONOCCUPYCARD_H

#include <QSet>

#include "cardtask.h"
#include "region.h"

class RegionOccupyCard : public CardTask {
 public:
  RegionOccupyCard();
  RegionOccupyCard(
      const QString &, QVector<Region *> &, QVector<Region *> &, std::int32_t numOfRegions);
  QVector<Region *> regions() const;
  std::int32_t numOfRegions() const;
  bool checkTask(Player *) const override;
  ~RegionOccupyCard(){};

  QVector<Region *> allRegions() const;
  void setAllRegions(const QVector<Region *> &allRegions);

 private:
  QVector<Region *> m_regions;
  std::int32_t m_numOfRegions;
  QVector<Region *> m_allRegions;
};

#endif  // REGIONOCCUPYCARD_H
