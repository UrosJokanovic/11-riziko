#ifndef PLAYER_H
#define PLAYER_H
#include <QSet>
#include <QString>
#include <QVector>

// #include "card.h"
// #include "territory.h"
#include "cardtask.h"

enum class Color {
  RED,
  GREEN,
  BLUE,
  YELLOW,
  PURPLE,
};

class Card;
class Territory;

class Player {
 public:
  Player();
  Player(QString name, Color color);
  const QString &name() const;

  void setName(const QString &newName);
  QString getName();
  Color color() const;
  void setColor(Color newColor);
  std::int32_t numOfTerritories() const;
  Player *playerToDestroy() const;
  void setPlayerToDestroy(Player *newPlayerToDestroy);

  void addCard(Card *);
  void addTerritory(Territory *);
  void removeTerritory(Territory *);
  void removeCard(Card *);
  int getTanks(Card *, Card *, Card *);
  void moveTanks(Territory *, Territory *, int);

  void addRandomTer(QString);
  QSet<QString> getRandomTer();
  void addCardTask(CardTask *);
  CardTask *cardTask() const;

  void removeAllTerritories();

  const QSet<Territory *> &territories() const;
  void setTerritories(const QSet<Territory *> &newTerritories);

  const QVector<Card *> &cards() const;
  void setCards(const QVector<Card *> &newCards);

  void setNumOfTanks(int num);
  int getNumOfTanks();
  QString toString();

 private:
  QString m_name;
  Color m_color;
  Player *m_playerToDestroy = nullptr;
  QVector<Card *> m_cards;
  QSet<Territory *> m_territories;
  CardTask *m_cardTask;
  int m_numOfTanksToAdd;
};

#endif  // PLAYER_H
