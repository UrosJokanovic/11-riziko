#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QTimer>

#include "game.h"
#include "initializer.h"

namespace Ui {
class MainWindow;
}

class Game;
class Initializer;

class MainWindow : public QMainWindow {
  Q_OBJECT

 public:
  explicit MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

 signals:
  void currentIndexChanged(QString);

 private slots:
  void on_pbJoinGame_clicked();

  void on_pbStart_clicked();

  void on_pbBack_clicked();

  void on_pbDrawCard_clicked();

  void on_pbAttack_clicked();

  void on_pbCards_clicked();

  void on_pbExitCards_clicked();

  void on_pbNextCard_clicked();

  void on_pbAddPlayer_clicked();

  void on_pbExit_clicked();

  void on_pbPlayMove_clicked();

  void on_pbExcangeCards_clicked();

  void updateCbTo(QString n);

  void updateCbNum(QString n);

  void startTimer();

  void timerTimeout();

  void on_pbNextPlayer_clicked();

  void on_Albania_clicked();

  void on_Estonia_clicked();

  void on_Austria_clicked();

  void on_Belarus_clicked();

  void on_Belgium_clicked();

  void on_Bosnia_clicked();

  void on_Bulgaria_clicked();

  void on_Croatia_clicked();

  void on_Czech_clicked();

  void on_Denmark_clicked();

  void on_Finland_clicked();

  void on_France_clicked();

  void on_Germany_clicked();

  void on_Greece_clicked();

  void on_Hungary_clicked();

  void on_Iceland_clicked();

  void on_Ireland_clicked();

  void on_Italy_clicked();

  void on_Latvia_clicked();

  void on_Lithuania_clicked();

  void on_Macedonia_clicked();

  void on_Moldova_clicked();

  void on_Netherlands_clicked();

  void on_Norway_clicked();

  void on_Poland_clicked();

  void on_Portugal_clicked();

  void on_Romania_clicked();

  void on_Serbia_clicked();

  void on_Slovakia_clicked();

  void on_Slovenia_clicked();

  void on_Spain_clicked();

  void on_Sweden_clicked();

  void on_Switzerland_clicked();

  void on_Ukraine_clicked();

  void on_UnitedKingdom_clicked();

  void on_pbMove_clicked();

 private:
  Ui::MainWindow *ui;
  QTimer *timer;
  Game *game;
  Initializer *init;
};

#endif  // MAINWINDOW_H
