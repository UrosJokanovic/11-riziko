#ifndef TERRITORY_H
#define TERRITORY_H
#include <QString>
#include <QVector>

#include "player.h"

class Player;

class Territory {
 public:
  Territory();
  Territory(QString name);

  ~Territory();

  const QString &name() const;
  int32_t numOfTanks() const;
  const QSet<Territory *> &neighborTerritories() const;

  void setNumOfTanks(int numberOfTanks);
  void setTerritories(const QSet<Territory *> &territories);
  void increaseNumOfTanks(std::int32_t);
  bool decreaseNumOfTanks(int32_t num);
  void setNeighborTerritories(const QSet<Territory *> &territories);

  Player *owner() const;
  void setOwner(Player *newOwner);

 private:
  QString m_name;
  int m_numOfTanks;
  QSet<Territory *> m_neighborTerritories;
  Player *m_owner;
};

#endif  // TERRITORY_H
