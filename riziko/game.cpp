#include "game.h"

Game::Game() = default;

void Game::setTerritories(QSet<Territory *> territories) { m_territories = territories; }
void Game::setCards(QSet<Card *> cards) { m_cards = cards; }
void Game::setRegions(QVector<Region *> regions) { m_regions = regions; }
void Game::setPlayers(QVector<Player *> players) { m_players = players; }

void Game::removePlayer(Player *p) {
  m_players.removeOne(p);
  // std::cout << "Успешно избрисао играчa: " << p->name().toStdString() << std::endl;
  delete p;
}

auto Game::players() const -> const QVector<Player *> & { return m_players; }

auto Game::cards() const -> const QSet<Card *> & { return m_cards; }

auto Game::territories() const -> const QSet<Territory *> & { return m_territories; }

auto Game::regions() const -> const QVector<Region *> & { return m_regions; }

auto Game::currentPlayer() const -> Player * { return m_currentPlayer; }

void Game::setCurrentPlayer(Player *newCurrentPlayer) { m_currentPlayer = newCurrentPlayer; }

auto Game::checkGoal(Player *p) -> bool { return p->cardTask()->checkTask(p); }

auto Game::attack(Territory *att, Territory *def) -> QVector<int> {
  std::int32_t numTanksAtt = att->numOfTanks();
  std::int32_t numTanksDef = def->numOfTanks();

  int attDice1 = QRandomGenerator::global()->bounded(1, 7);
  int attDice2 = QRandomGenerator::global()->bounded(1, 7);
  int attDice3 = QRandomGenerator::global()->bounded(1, 7);

  int defDice1 = QRandomGenerator::global()->bounded(1, 7);
  int defDice2 = QRandomGenerator::global()->bounded(1, 7);
  int defDice3 = QRandomGenerator::global()->bounded(1, 7);

  if (numTanksAtt == 2) {
    attDice2 = 0;
    attDice3 = 0;
  }
  if (numTanksAtt == 3) {
    attDice3 = 0;
  }

  if (numTanksDef == 1) {
    defDice2 = 0;
    defDice3 = 0;
  }

  if (numTanksDef == 2) {
    defDice3 = 0;
  }

  std::vector<int> attDices({attDice1, attDice2, attDice3});
  std::sort(attDices.begin(), attDices.end(), std::greater<int>());
  std::vector<int> defDices({defDice1, defDice2, defDice3});
  std::sort(defDices.begin(), defDices.end(), std::greater<int>());

  QVector<int> dices(
    {attDices[0], attDices[1], attDices[2], defDices[0], defDices[1], defDices[2]});

  if (attDices[0] > defDices[0]) {
    def->decreaseNumOfTanks(1);
  } else {
    att->decreaseNumOfTanks(1);
  }

  if (attDices[1] != 0 && defDices[1] != 0) {
    if (attDices[1] > defDices[1]) {
      def->decreaseNumOfTanks(1);
    } else {
      att->decreaseNumOfTanks(1);
    }
  }

  if (attDices[2] != 0 && defDices[2] != 0) {
    if (attDices[2] > defDices[2]) {
      def->decreaseNumOfTanks(1);
    } else {
      att->decreaseNumOfTanks(1);
    }
  }

  return dices;
}

auto Game::addTanksToTerritory(std::int32_t numOfTanks, Territory *t) -> std::int32_t {
  t->increaseNumOfTanks(numOfTanks);
  return numOfTanks;
}

Game::~Game() {
  for (auto m_territorie : m_territories) {
    delete m_territorie;
  }
  for (auto &m_region : m_regions) {
    delete m_region;
  }
  for (auto &m_player : m_players) {
    delete m_player;
  }
}
