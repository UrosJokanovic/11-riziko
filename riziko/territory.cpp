#include "territory.h"

#include <utility>

Territory::Territory() = default;

auto Territory::name() const -> const QString & { return m_name; }

auto Territory::numOfTanks() const -> int32_t { return m_numOfTanks; }

auto Territory::neighborTerritories() const -> const QSet<Territory *> & {
  return m_neighborTerritories;
}

void Territory::setNeighborTerritories(const QSet<Territory *> &territories) {
  for (auto t : territories) m_neighborTerritories.insert(t);
}

void Territory::setNumOfTanks(int numberOfTanks) { m_numOfTanks = numberOfTanks; }

void Territory::setTerritories(const QSet<Territory *> &territories) {
  m_neighborTerritories = territories;
}

void Territory::increaseNumOfTanks(std::int32_t num) { m_numOfTanks += num; }

auto Territory::owner() const -> Player * { return m_owner; }

void Territory::setOwner(Player *newOwner) { m_owner = newOwner; }

Territory::Territory(QString name) : m_name(std::move(name)), m_numOfTanks(1) {}

Territory::~Territory() = default;

auto Territory::decreaseNumOfTanks(int32_t num) -> bool {
  if (num > m_numOfTanks) return false;
  m_numOfTanks -= num;
  return true;
}
