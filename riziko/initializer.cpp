#include "initializer.h"

#include <QRandomGenerator>
#include <QVector>
#include <iostream>

#include "QMap"
#include "card.h"
#include "cardtask.h"
#include "destroyplayercard.h"
#include "player.h"
#include "regionoccupycard.h"
#include "territoryoccupycard.h"

class MissionCard;
class CardDestroyTroops;
int numOfTanksInit = 21;
QVector<Region *> regions;
QSet<Territory *> territories;
QSet<Card *> cardsT;
QMap<int, QString> territoryMap;

auto Initializer::makeAdjTerritories(QStringList &words, QHash<QString, Territory *> &all)
  -> QSet<Territory *> {
  QSet<Territory *> adj;
  for (auto &word : words) {
    adj.insert(all[word]);
  }
  return adj;
}

Initializer::Initializer() = default;

auto Initializer::initializerTerritory(QVector<Player *> &m_players) -> QSet<Territory *> {
  QVector<int> usedTerritories;
  usedTerritories.reserve(35);

  territoryMap.insert(1, "Albania");
  territoryMap.insert(2, "Austria");
  territoryMap.insert(3, "Belarus");
  territoryMap.insert(4, "Belgium");
  territoryMap.insert(5, "Bosnia");
  territoryMap.insert(6, "Bulgaria");
  territoryMap.insert(7, "Croatia");
  territoryMap.insert(8, "Czech");
  territoryMap.insert(9, "Denmark");
  territoryMap.insert(10, "Estonia");
  territoryMap.insert(11, "Finland");
  territoryMap.insert(12, "France");
  territoryMap.insert(13, "Germany");
  territoryMap.insert(14, "Greece");
  territoryMap.insert(15, "Hungary");
  territoryMap.insert(16, "Iceland");
  territoryMap.insert(17, "Ireland");
  territoryMap.insert(18, "Italy");
  territoryMap.insert(19, "Latvia");
  territoryMap.insert(20, "Lithuania");
  territoryMap.insert(21, "Macedonia");
  territoryMap.insert(22, "Moldova");
  territoryMap.insert(23, "Netherlands");
  territoryMap.insert(24, "Norway");
  territoryMap.insert(25, "Poland");
  territoryMap.insert(26, "Portugal");
  territoryMap.insert(27, "Romania");
  territoryMap.insert(28, "Serbia");
  territoryMap.insert(29, "Slovakia");
  territoryMap.insert(30, "Slovenia");
  territoryMap.insert(31, "Spain");
  territoryMap.insert(32, "Sweden");
  territoryMap.insert(33, "Switzerland");
  territoryMap.insert(34, "Ukraine");
  territoryMap.insert(35, "UnitedKingdom");

  for (int i = 0; i < 5; i++) {
    m_players[i]->setNumOfTanks(numOfTanksInit);
    int j = 0;
    while (j != 7) {
      int mapKeyGenerator = QRandomGenerator::global()->bounded(1, 36);
      if (usedTerritories.contains(mapKeyGenerator)) {
        continue;
      } else {
        usedTerritories.append(mapKeyGenerator);
        auto *t = new Territory(territoryMap[mapKeyGenerator]);
        territories.insert(t);
        m_players[i]->addTerritory(t);
        t->setOwner(m_players[i]);
        j++;
      }
    }
  }

  QString terrs("../resources/territories.txt");
  QFile inputFile(terrs);

  QString name;
  QString line;
  QStringList words;
  QSet<Territory *> neighbours;

  if (inputFile.open(QIODevice::ReadOnly)) {
    QTextStream input(&inputFile);
    while (!input.atEnd()) {
      line = input.readLine();
      words = line.split(',');
      name = words[0];
      words.erase(words.begin(), words.begin() + 1);

      for (auto t : territories) {
        if (t->name() == name) {
          for (auto word : words) {
            for (auto ter : territories) {
              if (ter->name() == word) {
                neighbours.insert(ter);
              }
            }
          }
        }
        t->setNeighborTerritories(neighbours);
        neighbours.clear();
      }
    }
  } else {
    std::cout << "Territories file path doesn't exist!" << std::endl;
  }
  return territories;
}

auto Initializer::initializeRegions() -> QVector<Region *> {
  QSet<Territory *> BalticT;
  QSet<Territory *> SouthernEuropeT;
  QSet<Territory *> CentralEuropeT;
  QSet<Territory *> WesternEuropeT;
  QSet<Territory *> ScandinaviaT;
  QSet<Territory *> EasternEuropeT;

  for (auto t : territories) {
    if (t->name() == "Finland" or t->name() == "Estonia" or t->name() == "Lithuania" or
        t->name() == "Latvia") {
      BalticT.insert(t);
    } else if (t->name() == "Germany" or t->name() == "Poland" or t->name() == "Czech" or
               t->name() == "Austria" or t->name() == "Hungary" or t->name() == "Slovakia" or
               t->name() == "Switzerland") {
      CentralEuropeT.insert(t);
    } else if (t->name() == "France" or t->name() == "Belgium" or t->name() == "Netherlands" or
               t->name() == "UnitedKingdom" or t->name() == "Ireland") {
      WesternEuropeT.insert(t);
    } else if (t->name() == "Sweden" or t->name() == "Norway" or t->name() == "Denmark" or
               t->name() == "Iceland") {
      ScandinaviaT.insert(t);
    } else if (t->name() == "Belarus" or t->name() == "Ukraine" or t->name() == "Moldova" or
               t->name() == "Romania" or t->name() == "Bulgaria") {
      EasternEuropeT.insert(t);
    } else {
      SouthernEuropeT.insert(t);
    }
  }

  auto *Baltic = new Region("Baltic", BalticT);
  auto *SouthernEurope = new Region("Southern Europe", SouthernEuropeT);
  auto *CentralEurope = new Region("Central Europe", CentralEuropeT);
  auto *WesternEurope = new Region("WesternEurope", WesternEuropeT);
  auto *Scandinavia = new Region("Scandinavia", ScandinaviaT);
  auto *EasternEurope = new Region("Eastern Europe", EasternEuropeT);

  regions.append(Baltic);          // 0
  regions.append(SouthernEurope);  // 1
  regions.append(CentralEurope);   // 2
  regions.append(WesternEurope);   // 3
  regions.append(Scandinavia);     // 4
  regions.append(EasternEurope);   // 5

  return regions;
}

void Initializer::initializeCardTasks(QVector<Player *> &players) {
  QVector<int> usedCardTasks;
  usedCardTasks.reserve(5);
  CardTask *cardTask = nullptr;
  bool playerGotHisTask = false;

  QVector<Region *> regions1{regions[0], regions[1]};
  QVector<Region *> regions2{regions[4], regions[1]};
  QVector<Region *> regions3{regions[2], regions[3]};
  QVector<Region *> regions4{regions[2], regions[5]};
  QVector<Region *> regions5{regions[3], regions[5]};
  QVector<Region *> regions6{regions[0], regions[3]};
  QVector<Region *> regions7{regions[0], regions[2]};
  QVector<Region *> regions8{regions[4]};
  QVector<Region *> regions9{regions[4], regions[3]};
  QVector<Region *> regions10{regions[4], regions[5]};
  QVector<Region *> regions11{regions[4], regions[5]};
  QVector<Region *> regions12{regions[4], regions[2]};

  QVector<Region *> allR = {regions[0], regions[1], regions[2], regions[3], regions[4], regions[5]};

  for (int i = 0; i < 5; i++) {
    playerGotHisTask = false;
    while (!playerGotHisTask) {
      int cardTaskGenerator = QRandomGenerator::global()->bounded(1, 20);
      if (usedCardTasks.contains(cardTaskGenerator)) {
        continue;
      } else {
        usedCardTasks.append(cardTaskGenerator);
        QString path = "../images/task_cards/" + QString::number(cardTaskGenerator) + ".png";
        if (cardTaskGenerator == 1) {
          cardTask = new RegionOccupyCard(path, regions1, allR, 2);
        } else if (cardTaskGenerator == 2) {
          cardTask = new RegionOccupyCard(path, regions2, allR, 2);
        } else if (cardTaskGenerator == 3) {
          cardTask = new RegionOccupyCard(path, regions3, allR, 2);
        } else if (cardTaskGenerator == 4) {
          cardTask = new RegionOccupyCard(path, regions4, allR, 2);
        } else if (cardTaskGenerator == 5) {
          cardTask = new RegionOccupyCard(path, regions5, allR, 2);
        } else if (cardTaskGenerator == 6) {
          cardTask = new RegionOccupyCard(path, regions6, allR, 3);
        } else if (cardTaskGenerator == 7) {
          cardTask = new RegionOccupyCard(path, regions7, allR, 2);
        } else if (cardTaskGenerator == 8) {
          cardTask = new RegionOccupyCard(path, regions8, allR, 2);
        } else if (cardTaskGenerator == 9) {
          cardTask = new RegionOccupyCard(path, regions9, allR, 2);
        } else if (cardTaskGenerator == 10) {
          cardTask = new RegionOccupyCard(path, regions10, allR, 2);
        } else if (cardTaskGenerator == 11) {
          cardTask = new RegionOccupyCard(path, regions11, allR, 3);
        } else if (cardTaskGenerator == 12) {
          cardTask = new RegionOccupyCard(path, regions12, allR, 2);
        } else if (cardTaskGenerator == 13) {
          cardTask = new TerritoryOccupyCard(path, 16);
        } else if (cardTaskGenerator == 14) {
          cardTask = new TerritoryOccupyCard(path, 22);
        } else if (cardTaskGenerator == 15) {
          cardTask = new DestroyPlayerCard(path, players[1]);
          players[i]->setPlayerToDestroy(players[1]);
        } else if (cardTaskGenerator == 16) {
          cardTask = new DestroyPlayerCard(path, players[3]);
          players[i]->setPlayerToDestroy(players[3]);
        } else if (cardTaskGenerator == 17) {
          cardTask = new DestroyPlayerCard(path, players[2]);
          players[i]->setPlayerToDestroy(players[2]);
        } else if (cardTaskGenerator == 18) {
          cardTask = new DestroyPlayerCard(path, players[0]);
          players[i]->setPlayerToDestroy(players[0]);
        } else if (cardTaskGenerator == 19) {
          cardTask = new DestroyPlayerCard(path, players[4]);
          players[i]->setPlayerToDestroy(players[4]);
        }
        players[i]->addCardTask(cardTask);
        // VRLO CUDAN DEO KODA, ALI NIKAKO GA NE DIRATI, JER SE BEZ NJEGA JAVLJA NEKI BAG //
        if (i == 4 && cardTaskGenerator <= 12) {
          for (auto r : regions) {
            continue;
          }
        }
        playerGotHisTask = true;
      }
    }
  }
}

auto Initializer::initializeCards() -> QSet<Card *> {
  QFile file("../images/territory_cards/cards.txt");
  QStringList list;
  int id;
  QString name;
  int typeId;

  if (file.open(QIODevice::ReadOnly)) {
    QTextStream text(&file);
    QString line = text.readLine();
    while (!text.atEnd()) {
      line = text.readLine();
      list = line.split(" ");
      id = list[0].toInt();
      name = list[1];
      typeId = list[2].toInt();

      Card *card;
      for (auto t : territories) {
        if (t->name() == name) {
          card = new Card(id, t);
        }
      }

      if (typeId == 0) {
        card->setCardType(CardType::CAVALRY);
      } else if (typeId == 1) {
        card->setCardType(CardType::CANNON);
      } else {
        card->setCardType(CardType::INFANTRY);
      }
      allCards[id] = card;
      cardsT.insert(card);
    }
    file.close();
    return cardsT;
  } else {
    std::cout << "Failed to open file: "
              << "../images/territory_cards/cards.txt" << std::endl;
  }
}
